# Cloudwatch logs

resource "aws_cloudwatch_log_group" "logs" {
  name              = "/maildrop"
  retention_in_days = "7"
}